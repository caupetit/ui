# UI SFML LIBRARY #

### What is this repository for? ###

* Simple library made to create gui fastly.
* Version 0.1
* Doc to come

### How do I get set up? ###

* Install SFML2 dependencies on linux: [https://github.com/SFML/SFML/wiki/Tutorial:-Installing-SFML-dependencies](https://github.com/SFML/SFML/wiki/Tutorial:-Installing-SFML-dependencies)

* Compile project:
```
git submodule init
git submodule update
cd SFML && cmake . && make
cmake . && make
```
* Linking

headers ares in `SFML/include` and `include`
libs ares in `SFML/lib` and `lib`

* Example of compilation

### Contribution guidelines ###

* To come.

### Who do I talk to? ###

* caupetit@student.42.fr