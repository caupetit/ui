#Change this if you need to target a specific CMake version
cmake_minimum_required(VERSION 2.6)
project(UI)

# Builds options
option(UI_BUILD_EXAMPLES "Enables examples builds"
  OFF)

# Define executable
set(UI_LIB_NAME "ui")
set(CMAKE_VERBOSE_MAKEFILE 1)
set(UI_LIB_DIR ${CMAKE_BINARY_DIR}/lib)

message(STATUS "ui created to: ${UI_LIB_DIR}/")

# Enable debug symbols by default
if(CMAKE_BUILD_TYPE STREQUAL "")
  set(CMAKE_BUILD_TYPE Debug)
endif()
# (you can also set it on the command line: -D CMAKE_BUILD_TYPE=Release)

# Set version information in a config.h file
set(${UI_LIB_NAME}_VERSION_MAJOR 0)
set(${UI_LIB_NAME}_VERSION_MINOR 1)

configure_file(
  "${PROJECT_SOURCE_DIR}/config.h.in"
  "${PROJECT_SOURCE_DIR}/include/config.h")

include_directories("${PROJECT_SOURCE_DIR}/include")

# Detect and add SFML
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
set(CMAKE_CXX_FLAGS "-std=c++0x")
set(SFML_ROOT "${CMAKE_SOURCE_DIR}/SFML" CACHE PATH "SFML dir, set it if you want tu use your own SFML directory")

#Find any version 2.X of SFML
find_package(SFML 2 REQUIRED graphics window audio system)
if(SFML_FOUND)
  include_directories(${SFML_INCLUDE_DIR})

  #link osx-frameworks
  if(APPLE)
    #!!!!!!!             IF OSX RELEASED SDK VERSION              !!!!!!!
    # copy SFML_ROOT/extlibs/*.frameworks to SFML_ROOT/extlibs/Frameworks
    #!!!!!!!													  !!!!!!!
	find_library(FREETYPE_FRAMEWORK	freetype PATH
	  ${SFML_ROOT}/extlibs/Frameworks
	  ${SFML_ROOT}/extlibs/libs-osx/Frameworks/)
	MESSAGE(STATUS "Freetype: ${FREETYPE_FRAMEWORK}")
  endif(APPLE)
endif(SFML_FOUND)

#Compile lib
subdirs(src)

if (${UI_BUILD_EXAMPLES})
  subdirs(example)
endif()

# CPack packaging
include(InstallRequiredSystemLibraries)
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/COPYING")
set(CPACK_PACKAGE_VERSION_MAJOR "${${UI_LIB_NAME}_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${${UI_LIB_NAME}_VERSION_MINOR}")
include(CPack)
