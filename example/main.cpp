
#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>
#include "UI/ui.hpp"

class GameObject : ui::Background
{
public:
	GameObject(void): Background(NULL), _s("Test Event")
	{
		this->setSize(1024, 768);

		this->setTexture(_texture);

		ui::LabelDelegateMap buttons = {
			{"print message", UI_DELEGATE(&GameObject::action, this)},
			{"enable void", UI_DELEGATE(&GameObject::action1, this)},
			{"disable void", UI_DELEGATE(&GameObject::action2, this)},
			{"void", NULL},
			{"exit", UI_DELEGATE(&GameObject::exit, this)}
		};
		_lst = ui::ButtonList::addVertical(this, buttons);
		ui::setMainArea(this);
	}

	~GameObject(void) {}

	void action (void)
	{
		std::cout << "Action (Does nothing)" << _s << std::endl;
	}

	void action1 (void)
	{
		_lst->getButtons()[3]->setListenEvents(true);
		std::cout << _s << "Event Test 2 Enable void button" << std::endl;
	}
	
	void action2 (void)
	{
		_lst->getButtons()[3]->setListenEvents(false);
		std::cout << _s << "Event Test 3 Disable void button" << std::endl;
	}

	void	exit(void)
	{
		std::exit(0);
	}

	std::string			_s;
	ui::ButtonList *	_lst;
};

int main(void)
{
	ui::Label::defaultFont.loadFromFile("./misc/king.ttf");
	ui::Button::defaultTexture.loadFromFile("./misc/button.png");
	ui::init(1024, 768, "Test UI");

	GameObject menu;
	ui::loop();
	return 0;
}
