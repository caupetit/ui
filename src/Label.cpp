#include "UI/Prefabs/Label.hpp"

sf::Font	ui::Label::defaultFont;
sf::Color	ui::Label::defaultColor = sf::Color(0, 0, 0);

/***  Constructors - Destuctors  ***/
ui::Label::Label(void):
	SystemArea(), _text()
{
	DEBUG("Created Label void");
}

ui::Label::Label(ui::SystemArea * parent):
	SystemArea(parent), _text()
{
	DEBUG("Created Label parent");
}

ui::Label::Label(const ui::Label & rhs):
	SystemArea(), _text()
{
	DEBUG("Created Label copy");
	*this = rhs;
}

ui::Label::~Label(void)
{
}

/***  Public functions  ***/
void	ui::Label::draw(void) const
{
	ui::Displayer::instance().getWindow().draw(_text);
}

ui::Label *	ui::Label::addDefault(ui::SystemArea *parent,
								   const std::string & str)
{
	ui::Label * label = new Label(parent);

	label->_text.setFont(ui::Label::defaultFont);
	label->_text.setColor(ui::Label::defaultColor);
	label->_text.setStyle(sf::Text::Regular);
	label->_text.setString(str);
	label->setSizeFromParent(0.8, 0.5);
	label->center();
	parent->addSonArea(label);
	return label;
}

/***  Setters  ***/
void	ui::Label::setString(const std::string & str)
{
	_text.setString(str);
	this->setCharacterHeight(_area.height);
	this->_updateText();
}

void	ui::Label::setPosition(const float & x, const float & y)
{
	SystemArea::setPosition(x, y);
	sf::FloatRect	bounds = _text.getLocalBounds();
	_text.setPosition(x, y + bounds.top - bounds.height);
}

void	ui::Label::setSize(const float & x, const float & y)
{
	SystemArea::setSize(x, y);
	this->_updateText();
}

void	ui::Label::setCharacterHeight(const float & x)
{
	_area.height = x;
	_text.setCharacterSize(x);
	_area.width = _text.getLocalBounds().width;
	this->_updateText();
}

void	ui::Label::setCharacterHeightFromParent(const float & ratio)
{
	this->setCharacterHeight(_parent->getSize().y * ratio);
}

/***  Private functions  ***/
void	ui::Label::_updateText(void)
{
	_text.setCharacterSize(_area.height);
	sf::FloatRect	bounds = _text.getLocalBounds();
	float	width = bounds.left + bounds.width;
	float	height = bounds.height;
	float	ratioX = _area.width / width;
	float	ratioY = _area.height / height;
	_text.setScale(ratioX, ratioY);
}

/***  Operators  ***/
ui::Label &	ui::Label::operator=(const ui::Label & rhs)
{
	SystemArea::operator=(rhs);
	_text = rhs._text;
	return *this;
}
