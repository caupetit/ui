#include "UI/Prefabs/ButtonList.hpp"

/***  Static Members  ***/

/***  Constructors - Destuctors  ***/
ui::ButtonList::ButtonList(void): SystemArea(), _buttons()
{
}

ui::ButtonList::ButtonList(ui::SystemArea * parent):
	SystemArea(parent), _buttons()
{
}

ui::ButtonList::ButtonList(const ui::ButtonList & src): SystemArea(src)
{
}

ui::ButtonList::~ButtonList(void)
{
	
}

/***  Public functions  ***/
void	ui::ButtonList::draw(void) const {}

void	ui::ButtonList::textAlignLeft(const float & ratio)
{
	int		size = _buttons.size();

	for (int i = 0 ; i < size ; i++)
	{
		_buttons[i]->textAlignLeft(ratio);
	}
}

void	ui::ButtonList::textAlignRight(const float & ratio)
{
	int			btnnb = _buttons.size();

	for (int i = 0 ; i < btnnb ; i++)
	{
		_buttons[i]->textAlignRight(ratio);
	}
}

/***  Static public functions  ***/
ui::ButtonList *	ui::ButtonList::addVertical(ui::SystemArea * parent,
												ui::LabelDelegateMap & btns)
{
	ui::ButtonList *	btl = new ui::ButtonList(parent);
	ui::Button *		btn;
	int					lbNb = btns.size();

	btl->setSizeFromParent(0.6, 0.8);
	btl->center();
	btl->_direction = ui::ButtonList::vertical;
	btl->_spacing = 0.25;
	for (int i = 0 ; i < lbNb ; i++)
	{
		btn = ui::Button::addDefault(btl, btns[i].first);
		btn->setClickAction(btns[i].second);
		btl->_buttons.push_back(btn);
	}
	btl->_placeButtons();
	parent->addSonArea(btl);
	return btl;
}

ui::ButtonList *	ui::ButtonList::addHorizontal(ui::SystemArea * parent,
												ui::LabelDelegateMap & btns)
{
	ui::ButtonList *	btl = new ui::ButtonList(parent);
	ui::Button *		btn;
	int					lbNb = btns.size();

	btl->setSizeFromParent(0.8, 0.15);
	btl->center();
	btl->_direction = ui::ButtonList::horizontal;
	btl->_spacing = 0.1;
	for (int i = 0 ; i < lbNb ; i++)
	{
		btn = ui::Button::addDefault(btl, btns[i].first);
		btn->setClickAction(btns[i].second);
		btl->_buttons.push_back(btn);
	}
	btl->_placeButtons();
	parent->addSonArea(btl);
	return btl;
}

/***  Setters  ***/
void	ui::ButtonList::setPosition(const float & x, const float & y)
{
	ui::SystemArea::setPosition(x, y);
	_placeButtons();
}

void	ui::ButtonList::setSize(const float & x, const float & y)
{
	ui::SystemArea::setSize(x, y);
	_placeButtons();
}

void	ui::ButtonList::setSpacing(const float & spacing)
{
	_spacing = spacing;
	_placeButtons();
}

void	ui::ButtonList::setTextTopPosition(const float & ratio)
{
	int		size = _buttons.size();

	for (int i = 0 ; i < size ; i++)
	{
		_buttons[i]->getLabel()->setTopFromParent(ratio);
	}
}

/***  Getters  ***/
ui::Buttons &	ui::ButtonList::getButtons(void)
{
	return _buttons;
}

/***  Private functions  ***/
void	ui::ButtonList::_placeButtons(void)
{
	int		btnNb = _buttons.size();
	sf::Vector2f	sizeRatios;
	sf::Vector2f	posRatios;

	if (_direction == ui::ButtonList::vertical)
	{
		sizeRatios = sf::Vector2f(1, 1.0 / btnNb - _spacing / btnNb);
		posRatios = sf::Vector2f(0, 1.0 / btnNb + _spacing / (btnNb * btnNb));
	}
	else
	{
		sizeRatios = sf::Vector2f(1.0 / btnNb - _spacing / btnNb, 1);
		posRatios = sf::Vector2f(1.0 / btnNb + _spacing / (btnNb * btnNb), 0);
	}
	for (int i = 0 ; i < btnNb ; i++)
	{
		_buttons[i]->setSizeFromParent(sizeRatios);
		_buttons[i]->setPositionFromParent(i * posRatios.x, i * posRatios.y);
	}
}

/***  Operators  ***/
ui::ButtonList &	ui::ButtonList::operator=(const ui::ButtonList & rhs)
{
	ui::SystemArea::operator=(rhs);
	_buttons = rhs._buttons;
	return *this;
}
