#include "UI/Prefabs/Banner.hpp"

/***  Static Members  ***/
sf::Color	ui::Banner::defaultColor = sf::Color(255, 252, 255, 255);
sf::Texture	ui::Banner::defaultTexture;

/***  Constructors - Destuctors  ***/
ui::Banner::Banner(const std::string & str):
	Background(), _label(NULL)
{
	_label = ui::Label::addDefault(this, str);
	DEBUG("Created Banner void-str");
}

ui::Banner::Banner(ui::SystemArea * parent, const std::string & str):
	Background(parent), _label(NULL)
{
	_label = ui::Label::addDefault(this, str);
	DEBUG("Created Banner parent-str");
}

ui::Banner::Banner(const ui::Banner & rhs):
	Background(), _label(NULL)
{
	*this = rhs;
}

ui::Banner::~Banner(void)
{
}

/***  Public functions  ***/
void	ui::Banner::textAlignLeft(const float & ratio)
{
		_label->setLeftFromParent(ratio);
}

void	ui::Banner::textAlignRight(const float & ratio)
{
		_label->setLeft(_area.left + _area.width -
						_label->getSize().x - _area.width * ratio);
}

/***  Static public functions  ***/
ui::Banner * ui::Banner::addDefault(ui::SystemArea * parent,
									 const std::string & str)
{
	ui::Banner *	banner = new Banner(parent, str);
	banner->setSizeFromParent(0.5, 0.2);
	banner->setTexture(ui::Banner::defaultTexture);
	banner->_shape.setFillColor(ui::Banner::defaultColor);
	banner->setTopFromParent(0.1);
	banner->horizontalCenter();
	parent->addSonArea(banner);
	return banner;
}

/***  Setters  ***/
void	ui::Banner::setPosition(const float & x, const float & y)
{
	Background::setPosition(x, y);
	_label->center();
}

void	ui::Banner::setSize(const float & x, const float & y)
{
	Background::setSize(x, y);
	_label->setCharacterHeightFromParent(0.4);
	_label->center();
}

ui::Label *	ui::Banner::getLabel(void)
{
	return _label;
}

void	ui::Banner::setString(const std::string & str)
{
	_label->setString(str);
}

/***  Getters  ***/

/***  Private functions  ***/

/***  Operators  ***/
ui::Banner &	ui::Banner::operator=(const ui::Banner & rhs)
{
	Background::operator=(rhs);
	_label = rhs._label;
	return *this;
}
