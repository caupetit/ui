#include "UI/SystemArea.hpp"

ui::SystemArea *			ui::SystemArea::mainArea = NULL;

/***  Namespace functions  ***/
void		ui::setMainArea(ui::SystemArea * area)
{
	if (ui::SystemArea::mainArea)
		ui::SystemArea::mainArea->setActive(false);
	area->setActive(true);
	ui::SystemArea::mainArea = area;
}

/***  Constructors - Destructors  ***/
ui::SystemArea::SystemArea(void):
	_area(), _parent(NULL), _sons(), _active(true), _listen(true)
{
	DEBUG("Created SystemArea void");
}

ui::SystemArea::SystemArea(ui::SystemArea * parent):
	_area(), _parent(parent), _sons(), _active(true), _listen(true)
{
	DEBUG("Created SystemArea parent");
}

ui::SystemArea::SystemArea(const ui::SystemArea & src)
{
	*this = src;
	DEBUG("Created SystemArea copy");
}

ui::SystemArea::~SystemArea(void)
{
	ui::EventManager::instance().removeAreaListener(this);
	int size = _sons.size();
	for (int i = 0 ; i < size ; i++)
		delete _sons[i];
	DEBUG("Deleted SystemArea");
}

/***  Public functions  ***/
ui::SystemArea *	ui::SystemArea::addSonArea(ui::SystemArea * son)
{
	_sons.push_back(son);
	son->_parent = this;
	return son;
}

bool	ui::SystemArea::contains(const sf::Vector2f & pos) const
{
	return _area.contains(pos.x, pos.y);
}


void	ui::SystemArea::setActive(bool active)
{
	int	size = _sons.size();

	this->_active = active;
	for (int i = 0 ; i < size ; i++)
		_sons[i]->setActive(active);
}

bool	ui::SystemArea::isActive(void) const
{
	return _active;
}

void	ui::SystemArea::setListenEvents(bool listen)
{
	int size = _sons.size();

	this->_listen = listen;
	for (int i = 0 ; i < size ; i++)
		_sons[i]->setListenEvents(listen);
}

bool	ui::SystemArea::isListening(void) const
{
	return _listen;
}

void	ui::SystemArea::setId(int id)
{
	_id = id;
}

int		ui::SystemArea::getId(void)
{
	return _id;
}

void	ui::SystemArea::listenGlobalEvent(sf::Event::EventType type,
										  const ui::DelegatePtr & dlg)
{
	ui::EventManager::instance().addGlobalListener(type, dlg, this);
}

void	ui::SystemArea::listenLocalEvent(sf::Event::EventType type,
										 const ui::DelegatePtr & dlg)
{
	ui::EventManager::instance().addLocalListener(type, dlg, this);
}

void	ui::SystemArea::listenExternEvent(sf::Event::EventType type,
										 const ui::DelegatePtr & dlg)
{
	ui::EventManager::instance().addExternListener(type, dlg, this);
}

void	ui::SystemArea::stopListenGlobalEvent(sf::Event::EventType type,
											  const ui::DelegatePtr & dlg)
{
	ui::EventManager::instance().removeGlobalListener(type, dlg, this);
}

void	ui::SystemArea::stopListenLocalEvent(sf::Event::EventType type,
										  const ui::DelegatePtr & dlg)
{
	ui::EventManager::instance().removeLocalListener(type, dlg, this);
}

void	ui::SystemArea::stopListenExternEvent(sf::Event::EventType type,
										  const ui::DelegatePtr & dlg)
{
	ui::EventManager::instance().removeExternListener(type, dlg, this);
}

void	ui::SystemArea::horizontalCenter(void)
{
	sf::Vector2f	parentPos = _parent->getPosition();
	sf::Vector2f	parentSize = _parent->getSize();

	this->setPosition(parentPos.x + parentSize.x / 2 - _area.width / 2,
					  _area.top);
}

void	ui::SystemArea::verticalCenter(void)
{
	sf::Vector2f	parentPos = _parent->getPosition();
	sf::Vector2f	parentSize = _parent->getSize();

	this->setPosition(_area.left,
					  parentPos.y + parentSize.y / 2 - _area.height / 2);
}

void	ui::SystemArea::center(void)
{
	sf::Vector2f	parentPos = _parent->getPosition();
	sf::Vector2f	parentSize = _parent->getSize();

	this->setPosition(parentPos.x + parentSize.x * 0.5 - _area.width / 2,
					  parentPos.y + parentSize.y * 0.5 - _area.height / 2);
}

void	ui::SystemArea::update(void)
{

}

/***  Setters  ***/
void	ui::SystemArea::setSize(const float & x, const float & y)
{
	_area.width = x;
	_area.height = y;
}

void	ui::SystemArea::setPosition(const float & x, const float & y)
{
	_area.left = x;
	_area.top = y;
}

void	ui::SystemArea::setPosition(const sf::Vector2f & pos)
{
	this->setPosition(pos.x, pos.y);
}

void	ui::SystemArea::setSize(const sf::Vector2f & size)
{
	this->setSize(size.x, size.y);
}

void	ui::SystemArea::setBounds(const sf::Vector2f & pos,
								  const sf::Vector2f & size)
{
	this->setBounds(pos.x, pos.y, size.x, size.y);
}

void	ui::SystemArea::setBounds(const float & posX, const float & posY,
								  const float & sizeX, const float & sizeY)
{
	this->setPosition(posX, posY);
	this->setSize(sizeX, sizeY);
}

void	ui::SystemArea::setWidth(const float & width)
{
	this->setSize(width, _area.height);
}

void	ui::SystemArea::setHeight(const float & height)
{
	this->setSize(_area.width, height);
}

void	ui::SystemArea::setLeft(const float & left)
{
	this->setPosition(left, _area.top);
}

void	ui::SystemArea::setTop(const float & top)
{
	this->setPosition(_area.left, top);
}

sf::Vector2f ui::SystemArea::getPosition(void) const
{
	return sf::Vector2f(_area.left, _area.top);
}

sf::Vector2f ui::SystemArea::getSize(void) const
{
	return sf::Vector2f(_area.width, _area.height);
}

void	ui::SystemArea::setPositionFromParent(const sf::Vector2f & ratio)
{
	this->setPositionFromParent(ratio.x, ratio.y);
}

void	ui::SystemArea::setPositionFromParent(const float & x, const float & y)
{
	sf::Vector2f	parentPos = _parent->getPosition();
	sf::Vector2f	parentSize = _parent->getSize();

	this->setPosition(parentPos.x + parentSize.x * x,
					  parentPos.y + parentSize.y * y);
}

void	ui::SystemArea::setSizeFromParent(const sf::Vector2f & scale)
{
	sf::Vector2f	parentSize = _parent->getSize();

	this->setSize(parentSize.x * scale.x, parentSize.y * scale.y);
}

void	ui::SystemArea::setSizeFromParent(const float & x, const float & y)
{
	sf::Vector2f	parentSize = _parent->getSize();

	this->setSize(parentSize.x * x, parentSize.y * y);
}

void	ui::SystemArea::setBoundsFromParent(const sf::Vector2f & pos,
								  const sf::Vector2f & size)
{
	this->setBoundsFromParent(pos.x, pos.y, size.x, size.y);
}

void	ui::SystemArea::setBoundsFromParent(const float & posX,
											const float & posY,
											const float & sizeX,
											const float & sizeY)
{
	this->setPositionFromParent(posX, posY);
	this->setSizeFromParent(sizeX, sizeY);
}

void	ui::SystemArea::setWidthFromParent(const float & ratio)
{
	this->setWidth(_parent->getSize().x * ratio);
}

void	ui::SystemArea::setHeightFromParent(const float & ratio)
{
	this->setHeight(_parent->getSize().y * ratio);
}

void	ui::SystemArea::setLeftFromParent(const float & ratio)
{
	this->setLeft(_parent->getPosition().x + _parent->getSize().x * ratio);
}

void	ui::SystemArea::setTopFromParent(const float & ratio)
{
	this->setTop(_parent->getPosition().y + _parent->getSize().y * ratio);
}

/***  Operators  ***/
ui::SystemArea &	ui::SystemArea::operator=(const ui::SystemArea & rhs)
{
	_area = rhs._area;
	_parent = rhs._parent;
	_sons = rhs._sons;
	return *this;
}

/*** Public static functions  ***/
void	ui::SystemArea::drawFromMain(void)
{
	ui::SystemArea::mainArea->_systemDraw();
}

void	ui::SystemArea::updateFromMain(void)
{
	ui::SystemArea::mainArea->_systemUpdate();
}

/***  Private functions  ***/
void	ui::SystemArea::_systemDraw(void)
{
	if (_active)
		this->draw();

	int	size = _sons.size();
	for (int i = 0 ; i < size ; i++)
	{
		_sons[i]->_systemDraw();
	}
}

void	ui::SystemArea::_systemUpdate(void)
{
	if (_active)
		this->update();

	int	size = _sons.size();
	for (int i = 0 ; i < size ; i++)
	{
		_sons[i]->_systemUpdate();
	}
}
