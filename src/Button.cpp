#include "UI/Prefabs/Button.hpp"

/***  Static Members  ***/
sf::Color	ui::Button::normalColor = sf::Color(100, 250, 50, 200);
sf::Color	ui::Button::selectedColor = sf::Color::White;
sf::Color	ui::Button::activeColor = sf::Color(100, 200, 200, 200);
sf::Texture	ui::Button::defaultTexture;

/***  Constructors - Destuctors  ***/
ui::Button::Button(void):
	Banner(""), Clickable()
{
	_shape.setFillColor(this->normalColor);
	DEBUG("Created Button void");
}

ui::Button::Button(ui::SystemArea * parent, const std::string & str):
	Banner(parent, str), Clickable()
{
	_shape.setFillColor(this->normalColor);
	DEBUG("Created Button parent-str");
}

ui::Button::Button(const ui::Button & src):
	Banner(src), Clickable(src)
{
	*this = src;
}

ui::Button::~Button(void) {}

/***  Public functions  ***/
void	ui::Button::draw(void) const
{
	ui::getWindow().draw(_shape);
}

void	ui::Button::init(void)
{
	this->setNormalColor(Button::normalColor);
	this->setActiveColor(Button::activeColor);
	this->setSelectedColor(Button::selectedColor);
	this->listenLocalEvent(sf::Event::MouseButtonPressed,
						   UI_DELEGATE(&Button::_mouseDown, this));
	this->listenLocalEvent(sf::Event::MouseButtonReleased,
						   UI_DELEGATE(&Button::_mouseUpIn, this));
	this->listenExternEvent(sf::Event::MouseButtonReleased,
						   UI_DELEGATE(&Button::_mouseUpOut, this));
	this->listenLocalEvent(sf::Event::MouseMoved,
						   UI_DELEGATE(&Button::_mouseMoveIn, this));
	this->listenExternEvent(sf::Event::MouseMoved,
						   UI_DELEGATE(&Button::_mouseMoveOut, this));
}

void	ui::Button::changeColor(const sf::Color & color)
{
	_shape.setFillColor(color);
}

/***  Static public functions  ***/
ui::Button *	ui::Button::addDefault(ui::SystemArea * parent,
									   const std::string & str)
{
	ui::Button *	button = new Button(parent, str);

	button->setSizeFromParent(0.5, 0.2);
	button->_shape.setTexture(&ui::Button::defaultTexture);
	button->_shape.setFillColor(ui::Button::normalColor);
	button->center();
	button->init();
	parent->addSonArea(button);
	return button;
}

/***  Setters  ***/

/***  Getters  ***/

/***  Private functions  ***/

/***  Operators  ***/
ui::Button & ui::Button::operator=(const ui::Button & rhs)
{
	ui::Banner::operator=(rhs);
	return *this;
}

/***  Static private functions  ***/
