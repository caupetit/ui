#include "UI/Clickable.hpp"

/***  Static Members  ***/

/***  Constructors - Destuctors  ***/
ui::Clickable::Clickable(void):
	_clickActionDelegate(NULL), _active(false), _selected(false),
	_normalColor(), _activeColor(0, 0, 0), _selectedColor()
{
	DEBUG("Created Clickable void");
}

ui::Clickable::Clickable(const ui::Clickable & src)
{
	*this = src;
}

ui::Clickable::~Clickable(void)
{
}

/***  Public functions  ***/
void	ui::Clickable::select(void)
{
	this->changeColor(this->_selectedColor);
	_active = false;
	_selected = true;
}

void	ui::Clickable::unselect(void)
{
	this->changeColor(this->_normalColor);
	_active = false;
	_selected = false;
}

void	ui::Clickable::activate(void)
{
	this->changeColor(this->_activeColor);
	_active = true;
	_selected = false;
}

bool	ui::Clickable::isActive(void) const
{
	return _active;
}

/***  Static public functions  ***/

/***  Setters  ***/
void	ui::Clickable::setClickAction(const ui::DelegatePtr & delegate)
{
	_clickActionDelegate = delegate;
}

void	ui::Clickable::setNormalColor(const sf::Color & color)
{
	_normalColor = color;
}

void	ui::Clickable::setActiveColor(const sf::Color & color)
{
	_activeColor = color;
}

void	ui::Clickable::setSelectedColor(const sf::Color & color)
{
	_selectedColor = color;
}

/***  Getters  ***/

/***  Private functions  ***/
void	ui::Clickable::_mouseUpIn(void)
{
	if (!this->_active)
		return;
	this->select();
	if (_clickActionDelegate)
		(*_clickActionDelegate)();
}

void	ui::Clickable::_mouseUpOut(void)
{
	if (!this->_active)
		return;
	this->unselect();
}

void	ui::Clickable::_mouseDown(void)
{
	this->activate();
}

void	ui::Clickable::_mouseMoveIn(void)
{
	if (!_selected && !_active)
		this->select();
}

void	ui::Clickable::_mouseMoveOut(void)
{
	if (_selected)
		this->unselect();
}

/***  Static private functions  ***/

/***  Operators  ***/
ui::Clickable &	ui::Clickable::operator=(const ui::Clickable & rhs)
{
	_clickActionDelegate = rhs._clickActionDelegate;
	_active = rhs._active;
	_selected = rhs._selected;
	_normalColor = rhs._normalColor;
	_activeColor = rhs._activeColor;
	_selectedColor = rhs._selectedColor;
	return *this;
}
