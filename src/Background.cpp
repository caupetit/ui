#include "UI/Prefabs/Background.hpp"

/***  Static Members  ***/

/***  Constructors - Destuctors  ***/
ui::Background::Background(void):
	SystemArea(), _shape()
{
}

ui::Background::Background(ui::SystemArea * parent):
	SystemArea(parent), _shape()
{
}

ui::Background::Background(const ui::Background & src):
	SystemArea(src)
{
	*this = src;
}

ui::Background::~Background(void)
{

}

/***  Public functions  ***/
void	ui::Background::draw(void) const
{
	sf::RenderWindow & win = ui::Displayer::instance().getWindow();

	win.draw(_shape);
}

/***  Static public functions  ***/

/***  Setters  ***/
void	ui::Background::setPosition(const float & x, const float & y)
{
	SystemArea::setPosition(x, y);
	_shape.setPosition(x, y);
}

void	ui::Background::setSize(const float & x, const float & y)
{
	SystemArea::setSize(x, y);
	_shape.setSize(sf::Vector2f(x, y));
}

void	ui::Background::setTexture(const sf::Texture & texture)
{
	_texture = texture;
	_shape.setTexture(&_texture);
}

/***  Getters  ***/
sf::RectangleShape & ui::Background::getShape(void)
{
	return _shape;
}
		
/***  Private functions  ***/

/***  Operators  ***/
ui::Background &	ui::Background::operator=(const ui::Background & rhs)
{
	ui::SystemArea::operator=(rhs);
	_shape = rhs._shape;
	return *this;
}
