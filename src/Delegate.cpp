#include "UI/Events/Delegate.hpp"

ui::Delegate::Delegate(void * instance, CallbackCaller function):
	_instance(instance) , _caller(function)
{
}

ui::Delegate::Delegate(const ui::Delegate & src)
{
	*this = src;
}

void ui::Delegate::operator()() const
{
	return (*_caller)(_instance);
}

bool	ui::Delegate::operator==(const ui::Delegate & rhs) const
{
	return _caller == rhs._caller;
}

ui::Delegate &	ui::Delegate::operator=(const ui::Delegate & rhs)
{
	_instance = rhs._instance;
	_caller = rhs._caller;
	return *this;
}

bool	ui::Delegate::equals(ui::Delegate * a, ui::Delegate *b)
{
	return *a == *b;
}
