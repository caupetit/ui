#include "UI/Displayer.hpp"

/***  Namespace functions  ***/
sf::RenderWindow &	ui::getWindow(void)
{
	return ui::Displayer::instance().getWindow();
}

/***  Static Members  ***/
ui::Displayer	ui::Displayer::_instance = ui::Displayer();
bool			ui::Displayer::_initialised = false;

/***  Constructors - Destuctors  ***/
ui::Displayer::Displayer(void): _win(NULL)
{
	DEBUG("Displayer Created");
}

ui::Displayer::~Displayer(void)
{
	DEBUG("Displayer Deleted");
	delete _win;
}

/***  Public functions  ***/
void		ui::Displayer::init(int width, int height, std::string name)
{
	if (_initialised)
	{
		DEBUG("Already initialised");
		return;
	}
	_win = new sf::RenderWindow(sf::VideoMode(width, height), name);
	_winDefaultSize.x = width;
	_winDefaultSize.y = height;
	this->_initialised = true;
}

void		ui::Displayer::init(sf::RenderWindow *renderWin)
{
	if (_initialised)
	{
		DEBUG("Already initialised");
		return;
	}
	_win = renderWin;
	_winDefaultSize.x = _win->getSize().x;
	_winDefaultSize.y = _win->getSize().y;
	this->_initialised = true;
}

sf::RenderWindow &	ui::Displayer::getWindow(void)
{
	return *_win;
}

sf::Vector2f	ui::Displayer::getWinRatio(void) const
{
	sf::Vector2u	curSize = _win->getSize();
	return sf::Vector2f(static_cast<float>(curSize.x) / _winDefaultSize.x,
						static_cast<float>(curSize.y) / _winDefaultSize.y);
}

sf::Vector2f	ui::Displayer::getDefaultSize(void) const
{
	return _winDefaultSize;
}

/***  Static public functions  ***/
ui::Displayer & ui::Displayer::instance(void)
{
	return _instance;
}

/***  Setters  ***/

/***  Getters  ***/

/***  Private functions  ***/

/***  Operators  ***/

/***  Static private functions  ***/
