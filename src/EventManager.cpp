#include "UI/Events.hpp"

typedef sf::Event::EventType EventType;

/***  Static Members  ***/
ui::EventManager	ui::EventManager::_instance = ui::EventManager();
ui::EvToDelegateMap	ui::EventManager::_defaults = {
	{EventType::Closed, UI_DELEGATE(&ui::EventManager::closeAction, &_instance)}
};

/***  Constructors - Destuctors  ***/
ui::EventManager::EventManager(void) : _evMap()
{
	DEBUG("EventManager Created");
}

ui::EventManager::~EventManager(void)
{
	DEBUG("EventManager Deleted");
}

/***  Public functions  ***/
void	ui::EventManager::addEvent(sf::Event::EventType type,
								const ui::DelegatePtr & ptr)
{
	auto it = _evMap.find(type);
	if (it != _evMap.end())
	{
		it->second = ptr;
	}
	else
		_evMap.emplace(type, ptr);
}

void	ui::EventManager::call(sf::Event event)
{
	ui::Displayer &	displayer = ui::Displayer::instance();
	sf::Vector2f	winRatio = displayer.getWinRatio();
	sf::Vector2i	pos = sf::Mouse::getPosition(displayer.getWindow());
	sf::Vector2f	realPos = sf::Vector2f(pos.x / winRatio.x,
										   pos.y / winRatio.y);

	static auto comparator = [&] (ui::SystemArea * area)
		{
			return area->contains(realPos);
		};
	
	static auto isActive = [] (ui::SystemArea * area)
		{
			return area->isActive() && area->isListening();
		};

	_globalListener.executeIf(event.type, isActive);
	_callExternsLocals(event.type, comparator);
	auto it = _evMap.find(event.type);
	if (it == _evMap.end())
		return ;
	(*it->second)();
}

void	ui::EventManager::setDefault(sf::Event::EventType type)
{
	auto it = _defaults.find(type);

	if (it == _evMap.end())
	{
		DEBUG("No Default action for this event");
		return ;
	}
	this->addEvent(type, it->second);
}

void	ui::EventManager::addGlobalListener(sf::Event::EventType type,
											const ui::DelegatePtr & delegate,
											ui::SystemArea * parent)
{
	_globalListener.add(type, delegate, parent);
}

void	ui::EventManager::addLocalListener(sf::Event::EventType type,
										   const ui::DelegatePtr & delegate,
										   ui::SystemArea * parent)
{
	_localListener.add(type, delegate, parent);
}

void	ui::EventManager::addExternListener(sf::Event::EventType type,
											const ui::DelegatePtr & delegate,
											ui::SystemArea * parent)
{
	_externListener.add(type, delegate, parent);
}

void	ui::EventManager::removeGlobalListener(sf::Event::EventType type,
											   const ui::DelegatePtr & delegate,
											   ui::SystemArea * area)
{
	_globalListener.removeDelegate(type, delegate, area);
}

void	ui::EventManager::removeLocalListener(sf::Event::EventType type,
											  const ui::DelegatePtr & delegate,
											  ui::SystemArea * area)
{
	_localListener.removeDelegate(type, delegate, area);
}

void	ui::EventManager::removeExternListener(sf::Event::EventType type,
											   const ui::DelegatePtr & delegate,
											   ui::SystemArea * area)
{
	_externListener.removeDelegate(type, delegate, area);
}

void	ui::EventManager::removeAreaListener(ui::SystemArea *area)
{
	_localListener.removeArea(area);
	_globalListener.removeArea(area);
	_externListener.removeArea(area);
}

void	ui::EventManager::closeAction(void)
{
	ui::Displayer::instance().getWindow().close();
}

/***  Static public functions  ***/
ui::EventManager & ui::EventManager::instance(void)
{
	return _instance;
}

/***  Setters  ***/

/***  Getters  ***/

int			ui::EventManager::getCurrentAreaId(void) const
{
	return _currentAreaId;
}

/***  Private functions  ***/
void	ui::EventManager::_callExternsLocals(
	sf::Event::EventType type,
	std::function<bool (ui::SystemArea *)> fu)
{
	auto locals = _localListener.getStored();
	auto it = locals.find(type);
	if (it != locals.end())
	{
		auto areaIt = it->second.begin();

		while (areaIt != it->second.end())
		{
			if (areaIt->first->isActive() && areaIt->first->isListening())
			{
				_currentAreaId = areaIt->first->getId();
				if (!fu(areaIt->first))
					_externListener.execute(type, areaIt->first);
				else
					_localListener.execute(type, areaIt->first);
			}
			areaIt++;
		}
	}	
}

/***  Operators  ***/

/***  Static private functions  ***/
