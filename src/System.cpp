
#include "UI/System.hpp"

void			ui::init(int width, int height, std::string name)
{
	ui::EventManager &	evManager = ui::EventManager::instance();

	ui::Displayer::instance().init(width, height, name);
	evManager.setDefault(sf::Event::Closed);
}


void			ui::loop(void)
{
	sf::RenderWindow &	win = ui::Displayer::instance().getWindow();

	while (win.isOpen())
	{
		win.clear(sf::Color::Black);
		ui::pollEvent(win);
		ui::SystemArea::updateFromMain();
		ui::SystemArea::drawFromMain();
		win.display();
	}
}

void			ui::pollEvent(void)
{
	sf::RenderWindow &	win = ui::Displayer::instance().getWindow();
	ui::pollEvent(win);
}

void			ui::pollEvent(sf::RenderWindow & win)
{
	ui::EventManager &	manager = ui::EventManager::instance();
	sf::Event			event;

	while (win.pollEvent(event))
	{
		manager.call(event);
	}
}
