#include "UI/Listener.hpp"
#include "UI/System.hpp"

ui::Listener::Listener(void): _stored() {}

ui::Listener::~Listener(void) {}

void	ui::Listener::add(sf::Event::EventType type,
					   const ui::DelegatePtr & delegate,ui::SystemArea * area)
{
	auto it = _stored.find(type);
	if (it == _stored.end())
	{
		EventAreaAction umap;
		umap.emplace(area, std::vector<ui::DelegatePtr> (1, delegate));
		_stored.emplace(type, umap);
		DEBUG("Listener::add: Added new");
	}
	else
	{
		auto areaIt = it->second.find(area);
		if (areaIt == it->second.end())
		{
			it->second.emplace(area,
							   std::vector<ui::DelegatePtr> (1, delegate));
			DEBUG("Listener::add: Added new area on event");
		}
		else
		{
			areaIt->second.push_back(delegate);
			DEBUG("Listener::add: Added new delegate on area");
		}
	}
}

void	ui::Listener::removeArea(ui::SystemArea * area)
{
	auto it = _stored.begin();
	while (it != _stored.end())
	{
		auto areaIt = it->second.find(area);
		if (areaIt != it->second.end())
		{
			DEBUG("RemoveArea deleted one type of " <<
				  areaIt->second.size() << " Delegates");
			it->second.erase(area);
		}
		else
			it++;
	}
}

void	ui::Listener::removeDelegate(sf::Event::EventType type,
								  const ui::DelegatePtr & dlg,
								  ui::SystemArea * area)
{
	auto it = _stored.find(type);
	if (it == _stored.end())
		return;
	auto aIt = it->second.find(area);
	if (aIt == it->second.end())
		return ;
	auto dIt = aIt->second.begin();
	for (; dIt != aIt->second.end() ; dIt++)
	{
		if (**dIt == *dlg)
		{
			aIt->second.erase(dIt);
			break;
		}
	}
}

void	ui::Listener::executeIf(sf::Event::EventType type,
								std::function<bool (ui::SystemArea *)> fu) const
{
	auto it = _stored.find(type);
	if (it != _stored.end())
	{
		auto areaIt = it->second.begin();
		while (areaIt != it->second.end())
		{
			if (fu(areaIt->first))
			{
				int size = areaIt->second.size();
				for (int i = 0 ; i < size ; i++)
					(*areaIt->second[i])();
			}
			areaIt++;
		}
	}
}

void	ui::Listener::execute(sf::Event::EventType type) const
{
	auto it = _stored.find(type);
	if (it != _stored.end())
	{
		auto areaIt = it->second.begin();
		while (areaIt != it->second.end())
		{
			int size = areaIt->second.size();
			for (int i = 0 ; i < size ; i++)
				(*areaIt->second[i])();
			areaIt++;
		}
	}
}

void	ui::Listener::execute(sf::Event::EventType type,
							  ui::SystemArea * area) const
{
	auto it = _stored.find(type);
	if (it != _stored.end())
	{
		auto areaIt = it->second.find(area);
		if (areaIt != it->second.end())
		{
			int size = areaIt->second.size();
			for (int i = 0 ; i < size ; i++)
				(*areaIt->second[i])();
		}
	}
}

const ui::Store &	ui::Listener::getStored(void) const
{
	return _stored;
}
