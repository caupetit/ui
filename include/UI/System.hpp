#ifndef UI_SYSTEM_HPP
# define UI_SYSTEM_HPP

# include <string>
# include <SFML/Window.hpp>
# include "UI/Events.hpp"
# include "UI/SystemArea.hpp"
# include "UI/Displayer.hpp"

namespace ui
{
	void	init(int width, int height, std::string name);
	void	loop(void);
	void	pollEvent(sf::RenderWindow & win);
	void	pollEvent(void);
}

#endif
