#ifndef UI_DISPLAYER_HPP
# define UI_DISPLAYER_HPP

#include <string>
#include <UI/Events.hpp>
#include <SFML/Graphics.hpp>

namespace ui
{
	class Displayer;

	sf::RenderWindow &	getWindow(void);
}

class ui::Displayer
{
public:
	void				init(int width, int height, std::string name);
	void				init(sf::RenderWindow * renderWin);
	sf::RenderWindow &	getWindow(void);

	sf::Vector2f		getWinRatio(void) const;
	sf::Vector2f		getDefaultSize(void) const;
	static ui::Displayer &	instance(void);

private:
	Displayer(void);
	Displayer(const Displayer & src);
	~Displayer(void);
	Displayer & operator=(const Displayer & rhs);

	sf::RenderWindow *		_win;
	sf::Vector2f			_winDefaultSize;
	static ui::Displayer	_instance;
	static bool				_initialised;
};

#endif
