#ifndef UI_LISTENER_HPP
# define UI_LISTENER_HPP

#include <unordered_map>
#include <vector>
#include <algorithm>

#include "SFML/Graphics.hpp"

#include "UI/Events/Delegate.hpp"
#include "UI/Events/DelegateFactory.hpp"

namespace ui
{
	class Listener;
	class SystemArea;

	typedef std::unordered_map<ui::SystemArea *,
							   std::vector<ui::DelegatePtr>,
							   std::hash<ui::SystemArea *> > EventAreaAction;

	typedef std::unordered_map<
		sf::Event::EventType,
		EventAreaAction,
		std::hash<int> > Store;
}

class ui::Listener
{
public:
	Listener(void);
	~Listener(void);

	void	add(sf::Event::EventType type, const ui::DelegatePtr & delegate,
				ui::SystemArea * parent);
	void	removeArea(ui::SystemArea * area);
	void	removeDelegate(sf::Event::EventType type, const ui::DelegatePtr & dlg,
							   ui::SystemArea * area);

	void	executeIf(sf::Event::EventType type,
					  std::function<bool (ui::SystemArea *)>) const;
	void	execute(sf::Event::EventType type) const;
	void	execute(sf::Event::EventType type, ui::SystemArea * area) const;


	const ui::Store &	getStored(void) const;

private:
	Listener(const Listener & src);

	Store		_stored;

	Listener & operator=(const Listener & rhs);
};

#endif
