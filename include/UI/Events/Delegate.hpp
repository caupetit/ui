#ifndef DELEGATE_HPP
# define DELEGATE_HPP

namespace ui
{
	class Delegate;
}

/**
 *	Delegate Class.
 *	Fonctor pattern to call a function that calls a method of _instance.
 *  Thanks to
 *  http://blog.coldflake.com/posts/C++-delegates-on-steroids/
 *	https://github.com/marcmo/delegates
 */
class ui::Delegate
{
	typedef void (*CallbackCaller)(void *instance);

public:
	Delegate(void * instance, CallbackCaller function);
	Delegate(const Delegate & src);

	void operator()() const;

	bool	operator==(const ui::Delegate & rhs) const;

	ui::Delegate &	operator=(const Delegate & rhs);

	static bool	equals(ui::Delegate * a, ui::Delegate *b);

private:

	void*			_instance;
	CallbackCaller	_caller;	
};

#endif
