#ifndef UI_DELEGATEFACTORY_HPP
# define UI_DELEGATEFACTORY_HPP

#include <memory>

#define UI_DELEGATE(func, instancePtr) \
	(ui::makeDelegate(func).Create<func>(instancePtr))

namespace ui
{
	template <typename T>
	struct DelegateFactory;

	typedef std::shared_ptr<Delegate> DelegatePtr;

	template<typename T>
	ui::DelegateFactory<T> makeDelegate(void (T::*)(void))
	{
		return ui::DelegateFactory<T>();
	}
}

/**
 *	DelegateFactory Class.
 *	Creates a delegate from a templated function that calls
 *  a given method from T.
 *  Thanks to
 *  http://blog.coldflake.com/posts/C++-delegates-on-steroids/
 *	https://github.com/marcmo/delegates
 */
template<typename T>
struct ui::DelegateFactory
{
    template<void (T::*foo)(void)>
    static void methodCaller(void* o)
	{
		return (static_cast<T*>(o)->*foo)();
	}

    template<void (T::*foo)(void)>
    inline static ui::DelegatePtr Create(T* o)
	{
		return std::make_shared<Delegate>(o, &DelegateFactory::methodCaller<foo>);
	}
};


# endif
