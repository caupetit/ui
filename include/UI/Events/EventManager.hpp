#ifndef UI_EVENT_MANAGER_HPP
# define UI_EVENT_MANAGER_HPP

#include <unordered_map>
#include <SFML/Window.hpp>
#include "UI/Events.hpp"
#include "UI/Displayer.hpp"
#include "UI/SystemArea.hpp"
#include "UI/Listener.hpp"

namespace ui
{
	class EventManager;
	class SystemArea;

	typedef std::unordered_map<
		sf::Event::EventType, ui::DelegatePtr, std::hash<int> > EvToDelegateMap;
};

class ui::EventManager
{
public:
	void		addEvent(sf::Event::EventType type,
						 const ui::DelegatePtr & ptr);
	void		call(sf::Event event);
	void		setDefault(sf::Event::EventType type);
	void		addGlobalListener(sf::Event::EventType type,
								  const ui::DelegatePtr & delegate,
								  ui::SystemArea * area);
	void		addLocalListener(sf::Event::EventType type,
								 const ui::DelegatePtr & delegate,
								 ui::SystemArea * area);
	void		addExternListener(sf::Event::EventType type,
								 const ui::DelegatePtr & delegate,
								 ui::SystemArea * area);
	void		removeGlobalListener(sf::Event::EventType type,
									const ui::DelegatePtr & delegate,
									 ui::SystemArea * area);
	void		removeLocalListener(sf::Event::EventType type,
									const ui::DelegatePtr & delegate,
									ui::SystemArea * area);
	void		removeExternListener(sf::Event::EventType type,
									const ui::DelegatePtr & delegate,
									ui::SystemArea * area);
	void		removeAreaListener(ui::SystemArea *area);

	int			getCurrentAreaId(void) const;
	
	static ui::EventManager & instance(void);
private:
	EventManager(void);
	EventManager(const ui::EventManager &);
	~EventManager(void);

	ui::EventManager & operator=(const ui::EventManager & rhs);

	void		_callExternsLocals(
		sf::Event::EventType type,
		std::function<bool (ui::SystemArea *)> fu);

	void	closeAction(void);

	EvToDelegateMap			_evMap;
	ui::Listener			_globalListener;
	ui::Listener			_localListener;
	ui::Listener			_externListener;
	int						_currentAreaId;
	
	static ui::EventManager	_instance;
	static EvToDelegateMap	_defaults;

};


#endif /* UI_EVENTS_MANAGER_HPP */
