#ifndef UI_SYSTEM_AREA_HPP
# define UI_SYSTEM_AREA_HPP

#include "SFML/Graphics.hpp"
#include "UI/Events.hpp"

namespace ui
{
	class SystemArea;

	void	setMainArea(ui::SystemArea * area);
}

class ui::SystemArea
{
public:
	SystemArea(void);
	SystemArea(ui::SystemArea * parent);
	SystemArea(const ui::SystemArea & src);
	virtual ~SystemArea(void);

	virtual bool		contains(const sf::Vector2f & pos) const;
	ui::SystemArea *	addSonArea(ui::SystemArea * son);

	void	setActive(bool active);
	bool	isActive(void) const;
	void	setListenEvents(bool listen);
	bool	isListening(void) const;

	void	setId(int id);
	int		getId(void);
	
	void	listenGlobalEvent(sf::Event::EventType type,
							  const ui::DelegatePtr & dlg);
	void	listenLocalEvent(sf::Event::EventType type,
							 const ui::DelegatePtr & dlg);
	void	listenExternEvent(sf::Event::EventType type,
							 const ui::DelegatePtr & dlg);
	void	stopListenGlobalEvent(sf::Event::EventType type,
								  const ui::DelegatePtr & dlg);
	void	stopListenLocalEvent(sf::Event::EventType type,
								 const ui::DelegatePtr & dlg);
	void	stopListenExternEvent(sf::Event::EventType type,
								 const ui::DelegatePtr & dlg);

	void	horizontalCenter(void);
	void	verticalCenter(void);
	void	center(void);

	virtual void	draw(void) const = 0;
	virtual void	update(void);

	virtual void	setPosition(const float & x, const float & y);
	virtual void	setSize(const float & x, const float & y);
	void			setPosition(const sf::Vector2f & pos);
	void			setSize(const sf::Vector2f & size);
	void			setBounds(const sf::Vector2f & pos,
							   const sf::Vector2f & size);
	void			setBounds(const float & posX, const float & posY,
							  const float & sizeX, const float & sizeY);
	void			setWidth(const float & width);
	void			setHeight(const float & height);
	void			setLeft(const float & left);
	void			setTop(const float & top);
	void			setPositionFromParent(const sf::Vector2f & ratios);
	void			setPositionFromParent(const float & x, const float & y);
	void			setSizeFromParent(const sf::Vector2f & scale);
	void			setSizeFromParent(const float & x, const float & y);
	void			setBoundsFromParent(const sf::Vector2f & pos,
										const sf::Vector2f & size);
	void			setBoundsFromParent(const float & posX,
										const float & posY,
										const float & sizeX,
										const float & sizeY);
	void			setWidthFromParent(const float & ratio);
	void			setHeightFromParent(const float & ratio);
	void			setLeftFromParent(const float & ratio);
	void			setTopFromParent(const float & ratio);

	sf::Vector2f	getPosition(void) const;
	sf::Vector2f	getSize(void) const;

	ui::SystemArea & operator=(const ui::SystemArea & rhs);

	static void			drawFromMain(void);
	static void			updateFromMain(void);
	static ui::SystemArea *			mainArea;

protected:
	void				_systemDraw(void);
	void				_systemUpdate(void);

	int								_id;
	sf::FloatRect					_area;
	ui::SystemArea *				_parent;
	std::vector<ui::SystemArea *>	_sons;
	bool							_active;
	bool							_listen;
};

#endif
