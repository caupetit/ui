#ifndef UI_CLICKABLE_HPP
# define UI_CLICKABLE_HPP

#include "SFML/Graphics.hpp"
#include "UI/SystemArea.hpp"

namespace ui
{
	class Clickable;
}

class ui::Clickable
{
public:
	Clickable(void);
	Clickable(const ui::Clickable & src);
	virtual ~Clickable(void);

	virtual void	changeColor(const sf::Color & color) = 0;
	virtual void	init(void) = 0;

	
	void		select(void);
	void		unselect(void);
	void		activate(void);
	bool		isActive(void) const;

	void		setClickAction(const ui::DelegatePtr & delegate);
	void		setNormalColor(const sf::Color & color);
	void		setActiveColor(const sf::Color & color);
	void		setSelectedColor(const sf::Color & color);

	ui::Clickable &	operator=(const ui::Clickable & rhs);

protected:
	void				_mouseUpIn(void);
	void				_mouseUpOut(void);
	void				_mouseDown(void);
	void				_mouseMoveIn(void);
	void				_mouseMoveOut(void);

	ui::DelegatePtr		_clickActionDelegate;
	bool				_active;
	bool				_selected;
	sf::Color 			_normalColor;
	sf::Color 			_activeColor;
	sf::Color			_selectedColor;
};

#endif
