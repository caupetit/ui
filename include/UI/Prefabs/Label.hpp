#ifndef UI_LABEL_HPP
# define UI_LABEL_HPP

#include <string>
#include "UI/SystemArea.hpp"

namespace ui
{
	class Label;
}

class ui::Label : public SystemArea
{
public:
	Label(void);
	Label(ui::SystemArea * parent);
	Label(const ui::Label & src);
	~Label(void);

	void	draw(void) const;

	static ui::Label *	addDefault(ui::SystemArea *parent,
								   const std::string & str);

	virtual void	setPosition(const float & x, const float & y);
	virtual void	setSize(const float & x, const float & y);
	void	setCharacterHeight(const float & x);
	void	setCharacterHeightFromParent(const float & ratio);
	void	setString(const std::string & str);

	ui::Label &	operator=(const ui::Label & rhs);

	static sf::Font		defaultFont;
	static sf::Color	defaultColor;
private:
	void		_updateText(void);

	sf::Text	_text;
};

#endif
