#ifndef UI_BUTTON_HPP
# define UI_BUTTON_HPP

#include "UI/SystemArea.hpp"
#include "UI/Clickable.hpp"
#include "UI/Prefabs/Banner.hpp"

namespace ui
{
	class Button;
}

class ui::Button : public ui::Banner, public ui::Clickable
{
public:
	Button(void);
	Button(ui::SystemArea * parent, const std::string & str);
	Button(const ui::Button & src);
	~Button(void);

	void		draw(void) const;
	void		init(void);
	void		changeColor(const sf::Color & color);

	static ui::Button *	addDefault(ui::SystemArea * parent,
								   const std::string & str);

	ui::Button & operator=(const ui::Button & rhs);

	static sf::Color	normalColor;
	static sf::Color	selectedColor;
	static sf::Color	activeColor;

	static sf::Texture	defaultTexture;

private:
};

#endif
