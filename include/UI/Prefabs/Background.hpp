#ifndef UI_BACKGROUND_HPP
# define UI_BACKGROUND_HPP

#include "UI/SystemArea.hpp"

namespace ui
{
	class Background;
}

class ui::Background : public ui::SystemArea
{
public:
	Background(void);
	Background(ui::SystemArea * parent);
	Background(const ui::Background & src);
	~Background(void);

	void	draw(void) const;

	using	SystemArea::setPosition;
	using	SystemArea::setSize;
	void	setPosition(const float & x, const float & y);
	void	setSize(const float & x, const float & y);
	void	setTexture(const sf::Texture & texture);
	
	sf::RectangleShape &	getShape(void);
	
	ui::Background & operator=(const ui::Background & rhs);

protected:
	sf::Texture				_texture;
	sf::RectangleShape		_shape;
};

#endif
