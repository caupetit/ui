#ifndef UI_BANNER_HPP
# define UI_BANNER_HPP

# include <string>
# include "UI/Prefabs/Background.hpp"
# include "UI/Prefabs/Label.hpp"

namespace ui
{
	class Banner;
}

class ui::Banner : public ui::Background
{
public:
	Banner(const std::string & str);
	Banner(ui::SystemArea * parent, const std::string & str);
	Banner(const ui::Banner & src);
	virtual ~Banner(void);

	void			textAlignLeft(const float & ratio);
	void			textAlignRight(const float & ratio);
	
	static ui::Banner *	addDefault(ui::SystemArea *parent,
									const std::string &str);
	
	virtual void	setPosition(const float & x, const float & y);
	virtual void	setSize(const float & x, const float & y);
	void			setString(const std::string & str);
	
	ui::Label *		getLabel(void);

	ui::Banner & operator=(const ui::Banner & rhs);

	static sf::Color	defaultColor;
	static sf::Texture	defaultTexture;

protected:
	Banner(void);

	ui::Label *			_label;
};

#endif
