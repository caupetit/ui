#ifndef UI_BUTTON_LIST_HPP
# define UI_BUTTON_LIST_HPP

#include "vector"
#include "UI/SystemArea.hpp"
#include "UI/Prefabs/Button.hpp"

namespace ui
{
	typedef std::vector<std::pair<std::string, ui::DelegatePtr> >	LabelDelegateMap;
	typedef std::vector<ui::Button *>								Buttons;

	class ButtonList;
}

class ui::ButtonList : public ui::SystemArea
{
	enum	e_direction
	{
		vertical, horizontal
	};
	
public:
	ButtonList(void);
	ButtonList(ui::SystemArea * parent);
	ButtonList(const ui::ButtonList & src);
	~ButtonList(void);

	void	draw(void) const;

	void			textAlignLeft(const float & ratio);
	void			textAlignRight(const float & ratio);
	
	static ui::ButtonList *	addVertical(ui::SystemArea * parent,
										 ui::LabelDelegateMap & btns);
	static ui::ButtonList *	addHorizontal(ui::SystemArea * parent,
										  ui::LabelDelegateMap & btns);

	virtual void	setPosition(const float & x, const float & y);
	virtual void	setSize(const float & x, const float & y);
	void			setSpacing(const float & spacing);
	void			setTextTopPosition(const float & ratio);

	ui::Buttons &	getButtons(void);

	ui::ButtonList & operator=(const ui::ButtonList & rhs);

private:
	void			_placeButtons(void);
	
	ui::Buttons		_buttons;
	e_direction		_direction;
	float			_spacing;
};

#endif
