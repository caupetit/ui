#ifndef UI_EVENT_HPP
# define UI_EVENT_HPP

#ifdef DEBUG_ON
# include <iostream>
# define CYAN		"\033[35m"
# define RESET		"\033[0m"
# define DEBUG(msg) std::cout << CYAN msg RESET << std::endl
# else
# define DEBUG(msg)
#endif

#include "Events/Delegate.hpp"
#include "Events/DelegateFactory.hpp"
#include "Events/EventManager.hpp"

#endif
